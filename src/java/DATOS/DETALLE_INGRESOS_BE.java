/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DATOS;  
/**
 *
 * @author TONY DC
 */
public class DETALLE_INGRESOS_BE {
    
    String codigodet;
    String codigoing;
    String producto;
    String cantidad;
    String precio;
    String subtotal;
    String igv;
    String total;

    public DETALLE_INGRESOS_BE() {
    }

    public DETALLE_INGRESOS_BE(String codigodet, String codigoing, String producto, String cantidad, String precio, String subtotal, String igv, String total) {
        this.codigodet = codigodet;
        this.codigoing = codigoing;
        this.producto = producto;
        this.cantidad = cantidad;
        this.precio = precio;
        this.subtotal = subtotal;
        this.igv = igv;
        this.total = total;
    }

    public String getCodigodet() {
        return codigodet;
    }

    public void setCodigodet(String codigodet) {
        this.codigodet = codigodet;
    }

    public String getCodigoing() {
        return codigoing;
    }

    public void setCodigoing(String codigoing) {
        this.codigoing = codigoing;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getIgv() {
        return igv;
    }

    public void setIgv(String igv) {
        this.igv = igv;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    
}
