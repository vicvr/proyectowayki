/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DATOS;  
/**
 *
 * @author TONY DC
 */
public class INGRESOS_BE {
    
    String codigo;
    String serie;
    String numero;
    String fecha;
    String proovedor;

    public INGRESOS_BE() {
    }

    public INGRESOS_BE(String codigo, String serie, String numero, String fecha, String proovedor) {
        this.codigo = codigo;
        this.serie = serie;
        this.numero = numero;
        this.fecha = fecha;
        this.proovedor = proovedor;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getProovedor() {
        return proovedor;
    }

    public void setProovedor(String proovedor) {
        this.proovedor = proovedor;
    }
    
}
