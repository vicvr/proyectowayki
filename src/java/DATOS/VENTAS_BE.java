/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DATOS;  
/**
 *
 * @author TONY DC
 */
public class VENTAS_BE {
    
    String codigo;
    String serie;
    String numero;
    String cliente;
    String trabajador;
    String fecha;

    public VENTAS_BE() {
    }

    public VENTAS_BE(String codigo, String serie, String numero, String cliente, String trabajador, String fecha) {
        this.codigo = codigo;
        this.serie = serie;
        this.numero = numero;
        this.cliente = cliente;
        this.trabajador = trabajador;
        this.fecha = fecha;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getTrabajador() {
        return trabajador;
    }

    public void setTrabajador(String trabajador) {
        this.trabajador = trabajador;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

}
