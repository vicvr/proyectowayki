/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DATOS;  
/**
 *
 * @author TONY DC
 */
public class DETALLE_VENTAS_BE {
    
    String codigodet;
    String codigoven;
    String producto;
    String precio;
    String cantidad;
    String subtotal;
    String descuento;
    String total;

    public DETALLE_VENTAS_BE() {
    }

    public DETALLE_VENTAS_BE(String codigodet, String codigoven, String producto, String precio, String cantidad, String subtotal, String descuento, String total) {
        this.codigodet = codigodet;
        this.codigoven = codigoven;
        this.producto = producto;
        this.precio = precio;
        this.cantidad = cantidad;
        this.subtotal = subtotal;
        this.descuento = descuento;
        this.total = total;
    }

    public String getCodigodet() {
        return codigodet;
    }

    public void setCodigodet(String codigodet) {
        this.codigodet = codigodet;
    }

    public String getCodigoven() {
        return codigoven;
    }

    public void setCodigoven(String codigoven) {
        this.codigoven = codigoven;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getDescuento() {
        return descuento;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

}
